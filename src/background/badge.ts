import { onMessage } from 'webext-bridge'

let count = 0

function setBadge(data: string | number) {
  let text = ''
  if (typeof data === 'string') {
    text = data
    count = 0
  }
  else {
    text = data < 1000 ? data.toString() : `${Math.floor(+data / 10) / 100}k`
  }

  browser.browserAction.setBadgeText({ text })
}

function incrementBadge() {
  setBadge(++count)
}
function decrementBadge() {
  const text = count <= 1 ? '' : --count
  setBadge(text)
}

function initBadge() {
  browser.browserAction.setBadgeBackgroundColor(
    { color: 'rgba(37, 99, 235)' },
  )

  onMessage('set-badge', ({ data }) => setBadge(data))

  onMessage('increment-badge', incrementBadge)

  onMessage('decrement-badge', decrementBadge)
}

export { initBadge, setBadge, incrementBadge, decrementBadge }
