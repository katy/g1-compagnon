import { sendMessage, onMessage, isInternalEndpoint } from 'webext-bridge'
import { Tabs } from 'webextension-polyfill'
import { initBadge, incrementBadge } from './badge'

initBadge()

// only on dev mode
if (import.meta.hot) {
  // @ts-expect-error for background HMR
  import('/@vite/client')
  // load latest content script
  import('./contentScriptHMR')
}

browser.runtime.onInstalled.addListener((): void => {
  // eslint-disable-next-line no-console
  console.log('Extension installed')
})

let previousTabId = 0

// communication example: send previous tab title from background page
// see shim.d.ts for type declaration
browser.tabs.onActivated.addListener(async({ tabId }) => {
  if (!previousTabId) {
    previousTabId = tabId
    return
  }

  let tab: Tabs.Tab

  try {
    tab = await browser.tabs.get(previousTabId)
    previousTabId = tabId
  }
  catch {
    return
  }

  // eslint-disable-next-line no-console
  console.log('previous tab', tab)
  sendMessage('tab-prev', { title: tab.title }, { context: 'content-script', tabId })
})

onMessage('get-current-tab', async() => {
  try {
    const tab = await browser.tabs.get(previousTabId)
    return {
      title: tab?.title,
    }
  }
  catch {
    return {
      title: undefined,
    }
  }
})

onMessage('fetch', async({ data, sender }) => {
  // Respond only if request is from 'devtools', 'content-script' or 'background' endpoint
  if (isInternalEndpoint(sender) && data) {
    const response = await fetch(data)
    return response.json()
  }
})

const connection = new WebSocket('wss://g1.data.e-is.pro/ws/event/user/2JggyyUn2puL5PG6jsMYFC2y9KwjjMmy2adnx3c5fUf8/fr-FR')

connection.onmessage = function(event) {
  console.log(event)
  incrementBadge()
}

connection.onopen = function(event) {
  console.log(event)
  console.log('Successfully connected to the echo websocket server...')
}
