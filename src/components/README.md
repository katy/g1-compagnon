## Components

Components in this dir will be auto-registered and on-demand, powered by [`vite-plugin-components`](https://github.com/antfu/vite-plugin-components).

Components can be shared in all views.

## Organization

https://dev.to/sanfra1407/how-to-organize-your-components-using-the-atomic-design-dj3

**Atoms**

Atoms are the smallest components of your application. Basically, they can be texts, buttons, form inputs and so on.
The golden rule is: if you can't split a component into smaller components then it must be an atom.

**Molecules**

Molecules are combinations of atoms bonded together. For example, if you have Text and Input atoms, you can combine them into a InputField (or whatever name you want) molecule .

**Organisms**

Organisms are combinations of molecules: if you mix two or more molecules you get an organism.


## Icons

You can use icons from almost any icon sets by the power of [Iconify](https://iconify.design/).

It will only bundle the icons you use. Check out [vite-plugin-icons](https://github.com/antfu/vite-plugin-icons) for more details.
