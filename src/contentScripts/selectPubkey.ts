import { onMounted } from 'vue'
import { sendMessage } from 'webext-bridge'

export function useSelectPubkey() {
  let hash = ''
  async function handleMouseup(event) {
    hash = getSelection()?.toString() || ''
    console.log({ test: hash, l: hash.length })
    if (hash.length === 43 || hash.length === 44) {
      const response = await sendMessage('fetch', `https://g1.data.adn.life:443/user/profile/_search?q=issuer:${hash}`, 'background')
      console.log('res', response)
    }
  }

  onMounted(() => {
    window.addEventListener('mouseup', handleMouseup)
  })
}
