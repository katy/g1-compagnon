import classWallet from './logic/classWallet'

declare const __DEV__: boolean

declare module '*.vue' {
  const component: any
  export default component
}

// https://blog.ninja-squad.com/2021/09/30/syntaxe-script-setup-en-vue-3/

declare global {
  // interface Wallet extends InstanceType<typeof classWallet> { }
  interface Wallet extends classWallet { }
}
