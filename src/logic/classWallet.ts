import * as g1lib from 'g1lib'

const COLORS_HSL = [
  [233, 47, 79], [88, 52, 67], [340, 85, 66], [291, 49, 60], [263, 49, 63],
  [232, 46, 64], [218, 93, 67], [187, 73, 70], [187, 73, 58], [15, 15, 69],
  [175, 43, 50], [151, 44, 53], [88, 53, 59], [66, 73, 59], [0, 0, 76],
  [51, 95, 53], [47, 100, 49], [40, 100, 50], [16, 100, 69], [262, 49, 74],
  [0, 0, 76], [201, 17, 62], [17, 16, 56], [0, 0, 64], [6, 71, 60], [187, 73, 70],
]

export default class Wallet {
  readonly publicKey!: string
  readonly pk: string
  readonly shortKey: string
  readonly checksum: string
  title?: string
  avatar?: string
  avatarPlaceholder?: {
    backgroundColor: string
    color: string
  }

  // private secretKey?: string

  constructor(data: string | Pick<Wallet, 'publicKey'>) {
    if (typeof data === 'string') data = { publicKey: data }
    Object.assign(this, data)

    this.pk = this.publicKey
    this.shortKey = this.getShortKey()
    this.checksum = this.getChecksum()
    this.avatarPlaceholder = this.getBackground()
  }

  getBackground() {
    // Convert pubkey to integer with sum of each letters charCode.
    let i = this.publicKey.length
    let sum = 0
    while (i--) sum += this.publicKey.charCodeAt(i)

    // Get HSL color for background and derive for text.
    const [hue, saturation, lightness] = COLORS_HSL[sum % COLORS_HSL.length]
    const textLightness = lightness < 55 ? lightness - 30 : lightness + 30
    return {
      backgroundColor: `hsl(${hue}, ${saturation}%, ${lightness}%)`,
      color: `hsl(${hue}, ${saturation}%, ${textLightness}%)`,
    }
  }

  getShortKey() {
    return g1lib.crypto.pubKey2shortKey(this.publicKey)
  }

  getChecksum() {
    return this.shortKey.split(':')[1]
  }
}
