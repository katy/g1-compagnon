import { useStorageLocal } from '~/composables/useStorageLocal'

export const storageDemo = useStorageLocal('webext-demo', 'Storage Demo', {
  listenToStorageChanges: true,
})

export const storageWallets = useStorageLocal('wallets', [], {
  listenToStorageChanges: true,
})
