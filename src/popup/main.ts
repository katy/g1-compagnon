import { createApp } from 'vue'
import {
  createWebHashHistory,
  createRouter,
} from 'vue-router'
import routes from 'voie-pages'

import App from './Popup.vue'
import '../styles'

const router = createRouter({

  history: createWebHashHistory(),
  routes,
})

createApp(App)
  .use(router)
  .mount('#app')
